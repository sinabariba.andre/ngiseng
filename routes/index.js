var express = require("express");
var router = express.Router();
const pageInventoryRouter = require("./inventory/pages.router");
const pelangganInventoryRouter = require("./inventory/pelanggan.router");
const userInventoryRouter = require("./inventory/users.router");
const barangInventoryRouter = require("./inventory/barang.router");

/* Router portfolio inventory. */
router.use("/", userInventoryRouter);
router.use("/portfolio/inventory", pageInventoryRouter);
router.use("/portfolio/inventory", pelangganInventoryRouter);
router.use("/portfolio/inventory", barangInventoryRouter);

router.get("/", (req, res) => {
  res.render("main_web/index");
});
// ======================================== //

router.get("/dummy-data-sparing", (req, res) => {
  const randomNum = () => Math.floor(Math.random() * 200);
  const data = [
    {
      id: 1,
      parameter: "Debit",
      range: randomNum(),
      unit: "01",
      satuan: "m3/s",
    },
    { id: 2, parameter: "TSS", range: randomNum(), unit: "01", satuan: "mg/L" },
    { id: 3, parameter: "pH", range: randomNum(), unit: "01", satuan: "mg/L" },
    {
      id: 4,
      parameter: "NO3N",
      range: randomNum(),
      unit: "01",
      satuan: "mg/L",
    },
    { id: 5, parameter: "PO4", range: randomNum(), unit: "01", satuan: "mg/L" },
    {
      id: 6,
      parameter: "NH3N",
      range: randomNum(),
      unit: "01",
      satuan: "mg/L",
    },
    { id: 7, parameter: "TDS", range: randomNum(), unit: "01", satuan: "mg/L" },
    {
      id: 8,
      parameter: "BOD 5",
      range: randomNum(),
      unit: "01",
      satuan: "mg/L",
    },
    { id: 9, parameter: "COD", range: randomNum(), unit: "01", satuan: "mg/L" },
    {
      id: 10,
      parameter: "Fe 5",
      range: randomNum(),
      unit: "01",
      satuan: "mg/L",
    },
    { id: 11, parameter: "Cu", range: randomNum(), unit: "01", satuan: "mg/L" },
    { id: 12, parameter: "Cr", range: randomNum(), unit: "01", satuan: "mg/L" },
  ];
  res.json(data)
});

module.exports = router;
