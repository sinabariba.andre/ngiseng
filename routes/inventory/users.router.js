const router = require("express").Router();
const userCtrl = require("../../controllers/inventory/user.controller");
const restrict = require("../../middlerwares/restrict");

router.post("/api/v1/auth/login", userCtrl.login);
router.get("/portfolio/inventory/dashboard",  userCtrl.dashboard);
router.get("/inventory/logout",  userCtrl.logout);


module.exports = router;
