const express = require('express')
const router = express.Router()
const pelangganCtrl = require('../../controllers/inventory/pelanggan.controller')

/* GET users listing. */
router.get('/pelanggan', pelangganCtrl.index)
router.get('/pelanggan/tambah', pelangganCtrl.tambah)
router.post('/pelanggan/tambah', pelangganCtrl.store)
router.get('/pelanggan/edit/:id', pelangganCtrl.edit)
router.post('/pelanggan/update/:id', pelangganCtrl.update)
router.post('/pelanggan/hapus/:id', pelangganCtrl.hapus)





module.exports =router