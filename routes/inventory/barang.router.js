const express = require("express");
const router = express.Router();
const barangCtrl = require("../../controllers/inventory/barang.controller");

/* GET users listing. */
router.get("/dt_barang", barangCtrl.dt_barang);
router.get("/barang", barangCtrl.index);
router.get("/barang/tambah", barangCtrl.tambah);
router.post("/barang/tambah", barangCtrl.store);
router.get("/barang/edit/:id", barangCtrl.edit);
router.post("/barang/update/:id", barangCtrl.update);
router.post("/barang/hapus/:id", barangCtrl.hapus);

// barang masuk
router.get("/barang/masuk", barangCtrl.masuk);
router.post("/barang/masuk", barangCtrl.store_masuk);
router.get("/barang-masuk/edit/:id", barangCtrl.edit_masuk);
router.post("/barang-masuk/update/:id", barangCtrl.update_masuk);
router.post("/barang-masuk/hapus/:id", barangCtrl.hapus_masuk);

// ==================

// Transaksi
router.get("/transaksi", barangCtrl.transaksi);
router.post("/transaksi", barangCtrl.transaksi_store);

// ==============================

module.exports = router;
