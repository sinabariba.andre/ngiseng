const flash = require('express-flash')
const {Pelanggan} = require('../../db/models')

const index = (req,res,next)=>{
    const sess = req.session
    const title =  'Pelanggan'
    
    Pelanggan.read().then((pelanggan)=>{
        console.log(sess)
        res.status(200).render('portfolio/inventory/pages/pelanggan/index', ({title,pelanggan,sess}))
    })
    
    
}
const tambah = (req,res)=>{
    const title =  'Tambah Pelanggan'
    res.status(200).render('portfolio/inventory/pages/pelanggan/tambah', ({title}))
}

const store = (req,res,next)=>{
    const sess = req.session
    Pelanggan.AddNew(req.body)
    .then((pelanggan)=>{
        
        res.status(200)
        // .cookie('tambah','pelanggan',{ expires: new Date(Date.now() + 9000)})
        .redirect('/portfolio/inventory/pelanggan',200,({sess}))
    })
    .catch((err)=>next(err.message)) 
    
}
const edit = (req,res)=>{
    const title =  'Edit Pelanggan'
    Pelanggan.findOne({where:{id:+req.params.id}}).then(pelanggan=>{
        res.status(200).render('portfolio/inventory/pages/pelanggan/edit', ({title,pelanggan}))
    })
}

const update = (req, res) => {
    // Mengambil properti dari request
    const {kapal, pengurus,telp}= req.body;
    Pelanggan.update(req.body,{
        where:{
            id:req.params.id
        }
    }).then((pelanggan)=>{
        res.status(200).redirect('/portfolio/inventory/pelanggan')
    }).catch((err)=>{
        res.status(400).json({
            message:err.message
        })
    })
};
const hapus = (req, res) => {
    
    
    Pelanggan.destroy({
        where:{
            id:req.params.id
        }
    }).then(pelanggan =>{
        res.locals.message= 'hapus'
        res.status(201).redirect('/portfolio/inventory/pelanggan', 200,({res:res.locals.message}))
    }).catch(err=>{
        res.status(400).json({
            message:err.message
        })
    })
}

module.exports={index,tambah,store,edit,update,hapus}