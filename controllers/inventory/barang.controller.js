const flash = require("express-flash");
const { Barang, Barang_masuk, Pelanggan } = require("../../db/models");
const sequelize = require("sequelize");

const dt_barang = async (req, res) => {
  const barang = await Barang.findAll().then((barang) => {
    return barang;
  });
  res.json(barang);
};

const index = async (req, res, next) => {
  const sess = req.session;
  const title = "Barang";
  const barang = await Barang.findAll().then((barang) => {
    return barang;
  });
  const barang_masuk = await Barang_masuk.findAll().then((barang) => {
    return barang;
  });
  console.log("object", barang);
  res.status(200).render("portfolio/inventory/pages/barang/index", {
    title,
    barang,
    barang_masuk,
    sess,
  });
};
const tambah = async (req, res) => {
  const title = "Tambah barang";
  let num = "0001";

  const id_barang = await Barang.findAll({ attributes: ["id"] }).then((id) => {
    if (null) {
      return (id = 0);
    } else {
      return id.length + num++;
    }
  });
  const kode = "BRG" + id_barang;
  res
    .status(200)
    .render("portfolio/inventory/pages/barang/tambah", { title, kode });
};

const store = (req, res, next) => {
  const sess = req.session;
  Barang.AddNew(req.body)
    .then((barang) => {
      res.status(200).redirect("/portfolio/inventory/barang");
    })
    .catch((err) => {
      console.log(err);
      next(err.message);
    });
};
const edit = async (req, res) => {
  const title = "Edit barang";
  let idB = req.params.id;
  let items;
  try {
    items = await Barang.findOne({ where: { id: req.params.id } });
    // res.json(barang)
  } catch (error) {
    return res.send("pages/default/error");
  }
  console.log(items);

  const locals = {
    data: {
      barang: items,
    },
    contentTitle: "Edit Barang",
    layout: "layouts/dashboard",
  };
  // res.render("portfolio/inventory/pages/barang/edit", { title, locals });
  res.json(locals.data);
  // return res.render("pages/dashboard/edit", locals);

  // Barang.findOne({ where: { id: +req.params.id } }).then((barang) => {
  //   res
  //     .status(200)
  //     .render("portfolio/inventory/pages/barang/edit", { title, barang });
  // });
};

const update = (req, res) => {
  // Mengambil properti dari request
  const { kode, nama_barang } = req.body;
  Barang.update(req.body, {
    where: {
      id: req.params.id,
    },
  })
    .then((barang) => {
      res.status(200).redirect("/portfolio/inventory/barang");
    })
    .catch((err) => {
      res.status(400).json({
        message: err.message,
      });
    });
};
const hapus = (req, res) => {
  Barang.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((barang) => {
      res.locals.message = "hapus";
      res.status(201).redirect("/portfolio/inventory/barang", 200, {
        res: res.locals.message,
      });
    })
    .catch((err) => {
      res.status(400).json({
        message: err.message,
      });
    });
};

// barang masuk
const masuk = async (req, res) => {
  const title = "Tambah barang masuk";
  const kode_brng = await Barang.findAll({
    attributes: ["kode", "nama_barang"],
  }).then((kode) => {
    return kode;
  });
  console.log("kode", kode_brng);
  res.status(200).render("portfolio/inventory/pages/barang/barang_masuk", {
    title,
    kode_brng,
  });
};

const store_masuk = async (req, res, next) => {
  // console.log(req.body)
  const stok_brng = await Barang.findOne({
    where: { kode: req.body.kode_barang },
  }).then((kode) => {
    return parseInt(kode.stok) + parseInt(req.body.jumlah_barang);
  });
  const update = await Barang.update(
    { stok: stok_brng },
    { where: { kode: req.body.kode_barang } }
  ).then((kode) => {
    return;
  });
  Barang_masuk.create(req.body)
    .then((barang) => {
      res.status(200).redirect("/portfolio/inventory/barang#barang-masuk");
    })
    .catch((err) => {
      next(err.message);
    });
};

const edit_masuk = (req, res) => {
  const title = "Edit barang masuk";
  Barang_masuk.findOne({ where: { id: +req.params.id } }).then((barang) => {
    res
      .status(200)
      .render("portfolio/inventory/pages/barang/edit_masuk", { title, barang });
  });
};

const update_masuk = async (req, res) => {
  // console.log(req.body)
  // Mengambil properti dari request
  const {
    tanggal,
    kode_barang,
    nama_barang,
    jumlah_barang,
    jmlh_before_update,
  } = req.body;
  const stok_brng = await Barang.findOne({
    where: { kode: kode_barang },
    attributes: ["stok"],
  }).then((stok) => {
    return stok.stok - jmlh_before_update;
  });
  const update_stok = await Barang.update(
    { stok: parseInt(stok_brng) + parseInt(jumlah_barang) },
    { where: { kode: kode_barang } }
  ).then(() => {
    return;
  });

  Barang_masuk.update(req.body, {
    where: {
      id: req.params.id,
    },
  })
    .then((barang) => {
      res.status(200).redirect("/portfolio/inventory/barang#barang-masuk");
    })
    .catch((err) => {
      res.status(400).json({
        message: err.message,
      });
    });
};
const hapus_masuk = async (req, res) => {
  const { kode_barang, jumlah_barang } = req.body;
  const stok_brng = await Barang.findOne({
    where: { kode: kode_barang },
    attributes: ["stok"],
  }).then((stok) => {
    return stok.stok - jumlah_barang;
  });
  console.log("asa", stok_brng);
  const update_stok = await Barang.update(
    { stok: stok_brng },
    { where: { kode: kode_barang } }
  ).then(() => {
    return;
  });
  const hapus_barang_masuk = await Barang_masuk.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((barang) => {})
    .catch((err) => {
      res.status(400).json({
        message: err.message,
      });
    });
  res.locals.message = "hapus";
  res.status(201).redirect("/portfolio/inventory/barang", 200, {
    res: res.locals.message,
  });
};

// controller transaksi
const transaksi = async (req, res, next) => {
  const title = "Transaksi";
  const pelanggan = await Pelanggan.findAll().then((data) => {
    return data;
  });
  const barang = await Barang.findAll().then((data) => {
    return data;
  });
  res.render("portfolio/inventory/pages/barang/transaksi", {
    title,
    pelanggan,
    barang,
  });
};

const transaksi_store = async (req, res) => {
  // get stok barang lama
  const stok_brng = await Barang.findOne({
    where: { kode: req.body.nama_barang },
    attributes: ["stok"],
  }).then((stok) => {
    return stok.stok - req.body.jumlah_barang;
  });

  // update stok
  const update_stok = await Barang.update(
    { stok: stok_brng },
    { where: { kode: req.body.nama_barang } }
  ).then((result) => {
    console.log(result);
  });
};

module.exports = {
  index,
  tambah,
  store,
  edit,
  update,
  hapus,
  masuk,
  store_masuk,
  edit_masuk,
  update_masuk,
  hapus_masuk,
  transaksi,
  transaksi_store,
  dt_barang,
};
