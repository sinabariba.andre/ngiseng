const{User} = require('../../db/models');
const bcrypt =require('bcrypt')
const passport = require('../../lib/passport');
    
const index = (req,res,next)=>{
    res.render('auth/login')
};
const showRegister = (req,res,next)=>{
    res.render('auth/register')
};
const register = (req,res,next)=>{
    // const {username, password}=req.body
    User.register(req.body)
    .then(()=>{
        res.redirect('/login')
    })
    .catch((err)=>next(err.message))

    // const encryptedPassword =bcrypt.hashSync(password)
    // User.create({
    //     username,
    //     password:encryptedPassword,
    // })
    // .then(()=>{
    //     res.redirect('/login')
    // })
    // .catch((err)=>next(err.message))
}

const showUserPage = (req,res)=>{
    res.render('user/index');
}
const login = passport.authenticate('local',{
    
    successRedirect:'/portfolio/inventory/dashboard',
    failureRedirect:'/portfolio/inventory',
    // session:false,
    failureFlash:true,
    successFlash:true
})
const dashboard = (req, res,next) => {
    
    /* req.user adalah instance dari User Model, hasil autentikasi dari passport. */
    const user= req.user;
    const title = "Dashboard"
    
    res.render('portfolio/inventory/pages/dashboard', {user,title})
    }

    const logout = (req,res)=>{
        req.logout();
        res.redirect('/portfolio/inventory');
    }

   
   

module.exports={ index,register,showRegister,login,showUserPage , dashboard, logout}
