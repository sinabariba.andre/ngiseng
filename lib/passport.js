const passport = require('passport');
const passportLocal = require('passport-local')
const LocalStrategy = passportLocal.Strategy
const { User } = require('../db/models')

const authenticate = async(username,password,done )=>{
    try{
        const user = await User.authenticate({username,password})
        return done(null,user)
    }catch(error){
        return done(null,false, {message:error.message})
    }
}

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
},authenticate))

passport.serializeUser((user, done) => done(null, user.id))
passport.deserializeUser(async(id,done) => done(null, await User.findByPk(id)))

/* Passport JWT Options */
// const options = {
//     // Untuk mengekstrak JWT dari request, dan mengambil token-nya dari header yang bernama Authorization
//      jwtFromRequest : ExtractJwt .fromHeader ('authorization' ),
//     /* Harus sama seperti dengan apa yang kita masukkan sebagai parameter kedua dari jwt.sign di User Model.
//      Inilah yang kita pakai untuk memverifikasi apakah tokennya dibuat oleh sistem kita */
//      secretOrKey : 'Ini rahasia ga boleh disebar-sebar' ,
//     }
//     passport .use(new JwtStrategy (options, async (payload, done) => {
//     // payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukkan di parameter pertama dari jwt.sign
//     User.findByPk (payload.id)
//      .then(user => done(null, user))
//      .catch(err => done(err, false))
//     }))
    

module.exports=passport