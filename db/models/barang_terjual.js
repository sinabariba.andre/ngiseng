'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Barang_Terjual extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Barang_Terjual.init({
    kode_barang: DataTypes.STRING,
    jumlah_barang: DataTypes.INTEGER,
    id_pelanggan: DataTypes.INTEGER,
    kapal: DataTypes.STRING,
    pengurus: DataTypes.STRING,
    telp: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Barang_Terjual',
  });
  return Barang_Terjual;
};