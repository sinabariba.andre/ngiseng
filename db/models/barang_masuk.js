'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Barang_masuk extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    static AddNew = ({ tanggal,kode_barang, nama_barang, jumlah_barang }) => {
      return this.create({ tanggal,kode_barang, nama_barang, jumlah_barang });
    };
  };
  Barang_masuk.init({
    tanggal: DataTypes.DATE,
    kode_barang: DataTypes.STRING,
    nama_barang: DataTypes.STRING,
    jumlah_barang: DataTypes.INTEGER,
    id_admin: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Barang_masuk',
  });
  return Barang_masuk;
};