'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pelanggan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static read(){
      return this.findAll()
    }

    static AddNew=({kapal,pengurus,telp})=>{
      return this.create({
        kapal,
        pengurus,
        telp
      })
    }

  };
  Pelanggan.init({
    kapal: DataTypes.STRING,
    pengurus: DataTypes.STRING,
    telp: DataTypes.STRING,
    id_admin: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Pelanggan',
  });
  return Pelanggan;
};