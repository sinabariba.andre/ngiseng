'use strict';
const fs = require('fs');
const bcrypt = require('bcrypt');
const encrypt = (password)=>{
  return bcrypt.hashSync(password, 10)
 }

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const newData = [];
    const rawData = fs.readFileSync('masterdata/user.json');
    const userData = JSON.parse(rawData);
    userData.map((user)=>{
      const seedData={
        username:user.username,
        email:user.email,
        password:encrypt(user.password),
        role:user.role,
        createdAt: new Date(),
        updatedAt:new Date()
      }
      newData.push(seedData)
    })
  
      await queryInterface.bulkInsert('Users', newData);
    },
  
    down: async (queryInterface, Sequelize) => {
      await queryInterface.bulkDelete('Users', null, {
        truncate:true,
        restartIdentity:true
      });
    }
  };
  