var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const session = require("express-session");
const flash = require("connect-flash");
const toastr = require("express-toastr");
const passport = require("./lib/passport");
const cors = require("cors");
// const baseUrl = 'localhost:3000/' ;

var indexRouter = require("./routes/index");
// var usersRouter = require('./routes/users');

var app = express();
app.use(logger("dev"));
app.use(express.json());
// app.options("*", cors({ origin: 'https://sinabariba-andre.herokuapp.com/', optionsSuccessStatus: 200 }));
// app.use(cors({ origin: "https://sinabariba-andre.herokuapp.com/", optionsSuccessStatus: 200 }));
app.use(cors());
app.use(express.urlencoded({ extended: false }));

// middlere
// const { createProxyMiddleware } = require("http-proxy-middleware");
// app.use(
//   "/api",
//   createProxyMiddleware({
//     target: "https://sinabariba-andre.herokuapp.com/", //original url
//     changeOrigin: true,
//     // secure: false,
//     onProxyRes: function (proxyRes, req, res) {
//       proxyRes.headers["Access-Control-Allow-Origin"] = "*";
//       proxyRes.header["Access-Control-Allow-Headers"] = "X-Requested-With";
//     },
//   })
// );
// Menggunakan Middleware Session
app.use(
  session({
    secret: "Top Secret",
    resave: false,
    saveUninitialized: false,
  })
);

// Menggunakan Middleware Passport
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());
app.use(toastr());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
